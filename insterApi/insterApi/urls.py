"""insterApi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from my_app.rest_view import LoggedAPIView, EntityAPIListView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/logged/', LoggedAPIView.as_view()),
    path('api/login/', obtain_auth_token, name='api_token_auth'),
    path('api/entity/', EntityAPIListView.as_view(), name='entity_api_list'),
]
