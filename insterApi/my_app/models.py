from django.db import models
class Entity(models.Model):
    x = models.CharField(max_length=255)
    y1 = models.CharField(blank=True, null=True, max_length=255)
    y2 = models.CharField(blank=True, null=True, max_length=255)
    y3 = models.CharField(blank=True, null=True, max_length=255)
    y4 = models.CharField(blank=True, null=True, max_length=255)
    y5 = models.CharField(blank=True, null=True, max_length=255)
    dataType = models.CharField(blank=True, null=True, max_length=255)