import csv
from typing import Dict

from django.core.management.base import BaseCommand

from my_app.models import Entity

class Command(BaseCommand):
    help = 'Import data from CSV file'

    def add_arguments(self, parser):
        parser.add_argument('path', nargs='+', type=str)
        parser.add_argument('datatype', nargs='+', type=str)

    def handle(self, *args, **options):
        filepath = options['path'][0]
        datatype = options['datatype'][0]
        with open(filepath, "r") as file:
            reader = csv.DictReader(file, delimiter=";")
            for row in reader:
                Entity.objects.create(x=row["X"], y1=row["Y1"],y2=row["Y2"],y3=row["Y3"],y4=row["Y4"],y5=row["Y5"],dataType=datatype)
        return "Success"