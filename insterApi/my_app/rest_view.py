from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework.generics import ListAPIView
from my_app.models import Entity

class LoggedAPIView(APIView):   
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        return Response(data={"user": self.request.user.username})

class EntityReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ["x", "y1", "y2", "y3", "y4", "y5"]


class EntityAPIListView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EntityReadSerializer

    def get_queryset(self):
        print(self.request.GET)
        if "dataType" in self.request.GET and self.request.GET["dataType"] == "a":
            return Entity.objects.filter(dataType=self.request.GET["dataType"])
        elif "dataType" in self.request.GET and self.request.GET["dataType"] == "b":
            return Entity.objects.filter(dataType=self.request.GET["dataType"])
        else:
            return []